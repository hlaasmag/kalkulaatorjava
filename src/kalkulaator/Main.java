/**
 * Kalkulaator
 * 
 * Peamine klass, kus luuakse graafikaobjektid, m22ratakse nende asukoht ja
 * kutsutakse v2lja trigonomeetriaarvutused
 *
 * 
 * @author Hando Laasmagi
 * @version 1.8
 * @date 10/12/2015
 */
package kalkulaator;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.scene.layout.VBox;

/**
 * Application klassist saame JavaFX-i p6hifunktsioonid
 *
 */
public class Main extends Application {
	static Stage aken;
	static Scene vaade1, vaade2;
	static TextField vastuseKast, arvutuseKuvamine;
	static Button vaade2to1, button, nupp, koma, clear, korruta;
	static Button lahuta, v6rdub, plussmin, jaga, liida;
	static Button[] numbriNupud = new Button[10];
	static double num1, num2, vastus;
	static int tehteKlikkideArv = 0;
	static char tehe;
	static boolean tehteSisestusLubatud = false, vastuseNuppLubatud = false;

	Button siinus, koosinus, tangens, cotangens;
	double trigoSisend;
	double trigoVastus;

	/**
	 * Antud meetod on vajalik, et JavaFX saaks Application klassi kaudu
	 * k2ivitada start meetodi.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * Start meetod on JavaFX-i kasutavate programmide alguspunkt. Siit
	 * kutsutakse v2lja k6ik Main klassi meetodid, millega luuakse graafilised
	 * objektid, m22ratakse nende asukoht ning pannakse kylge kujundus.
	 * 
	 * Veel kutsutakse siit v2lja Arvutamise klassi meetod nupuVajutusTegevus,
	 * millega m22ratakse, mis juhtub kui kasutaja vajutab kalkulaatori nuppe.
	 * 
	 * Lisaks kutsutakse siit meetodist v2lja trigonomeetriliste funktsioonide
	 * arvutamine.
	 * 
	 * @param primaryStage
	 */
	@Override
	public void start(Stage primaryStage) {
		GridPane kujundus1 = new GridPane();
		VBox kujundus2 = new VBox();
		vaadeteKujundus(kujundus1, kujundus2);
		nuppudeLoomine();
		vastuseAknaLoomine();
		nuppudeAsukohad(kujundus1, kujundus2);
		nuppudeSuurused();
		Arvutamine.nupuVajutusTegevus();
		arvutaTrigoFunktsioon();

		aken = primaryStage;
		primaryStage.setScene(vaade1);
		primaryStage.setTitle("Kalkulaator");
		primaryStage.show();
	}

	/**
	 * M22rab nuppude omavahelised kaugused ning lisab m6lemale stseenile CSS
	 * kujundusfaili
	 * 
	 * @param kujundus1 - esimese stseeni ylesehitus
	 * @param kujundus2 - teise stseeni ylesehitus
	 */
	public void vaadeteKujundus(GridPane kujundus1, VBox kujundus2) {
		kujundus1.setAlignment(Pos.CENTER);
		kujundus1.setHgap(7);
		kujundus1.setVgap(10);
		kujundus1.setPadding(new Insets(30, 30, 30, 30));
		kujundus2.setAlignment(Pos.CENTER);
		kujundus2.setSpacing(30);
		kujundus1.getStylesheets().add("kujundus/Kujundus.css");
		kujundus2.getStylesheets().add("kujundus/Kujundus.css");
	}

	/**
	 * Loob k6ikide nuppudeobjektid ja lisab neile peale kirja. Lisaks annab
	 * m6nedele nuppudele kylge vaikimisi kujundusest erineva kujunduse
	 */
	public void nuppudeLoomine() {
		// Loob numbritele 0-9 uued nupuobjektid ja salvestab need massiivi
		for (int i = 0; i < 10; i++) {
			numbriNupud[i] = new Button(Integer.toString(i));
		}
		koma = new Button(".");
		clear = new Button("C");
		korruta = new Button("*");
		lahuta = new Button("-");
		v6rdub = new Button("=");
		plussmin = new Button("+/-");
		jaga = new Button("/");
		liida = new Button("+");
		siinus = new Button("sin");
		koosinus = new Button("cos");
		tangens = new Button("tan");
		cotangens = new Button("cot");
		vaade2to1 = new Button("Tagasi arvutama");

		siinus.getStyleClass().add("button-trigo");
		koosinus.getStyleClass().add("button-trigo");
		tangens.getStyleClass().add("button-trigo");
		cotangens.getStyleClass().add("button-trigo");
		clear.getStyleClass().add("button-tehted");
		korruta.getStyleClass().add("button-tehted");
		lahuta.getStyleClass().add("button-tehted");
		v6rdub.getStyleClass().add("button-tehted");
		plussmin.getStyleClass().add("button-tehted");
		jaga.getStyleClass().add("button-tehted");
		liida.getStyleClass().add("button-tehted");
	}

	/**
	 * Loob v2ljad, kus kuvatakse arvutamise vastust ning arvutusk2iku,
	 * lisatakse stiil
	 * 
	 */
	public void vastuseAknaLoomine() {
		vastuseKast = new TextField("");
		vastuseKast.getStyleClass().add("textfield-vastus");
		vastuseKast.setEditable(false);

		arvutuseKuvamine = new TextField("");
		arvutuseKuvamine.setEditable(false);
		arvutuseKuvamine.getStyleClass().add("textfield-arvutus");
	}

	/**
	 * Graafilised objektid lisatakse ridadesse ja tulpadesse M22ratakse
	 * stseenide suurused
	 * 
	 * @param kujundus1
	 * @param kujundus2
	 */
	public void nuppudeAsukohad(GridPane kujundus1, VBox kujundus2) {
		// M22rame nupud ridadesse ja tulpadesse
		kujundus1.add(arvutuseKuvamine, 0, 0, 6, 1);
		kujundus1.add(vastuseKast, 0, 1, 6, 1);
		kujundus1.add(numbriNupud[1], 0, 2);
		kujundus1.add(numbriNupud[4], 0, 3);
		kujundus1.add(numbriNupud[7], 0, 4);
		kujundus1.add(numbriNupud[0], 0, 5, 2, 1);
		kujundus1.add(numbriNupud[2], 1, 2);
		kujundus1.add(numbriNupud[5], 1, 3);
		kujundus1.add(numbriNupud[8], 1, 4);
		kujundus1.add(numbriNupud[3], 2, 2);
		kujundus1.add(numbriNupud[6], 2, 3);
		kujundus1.add(numbriNupud[9], 2, 4);
		kujundus1.add(koma, 2, 5);
		kujundus1.add(clear, 3, 2);
		kujundus1.add(korruta, 3, 3);
		kujundus1.add(lahuta, 3, 4);
		kujundus1.add(v6rdub, 3, 5, 2, 1);
		kujundus1.add(plussmin, 4, 2);
		kujundus1.add(jaga, 4, 3);
		kujundus1.add(liida, 4, 4);
		kujundus1.add(siinus, 5, 2);
		kujundus1.add(koosinus, 5, 3);
		kujundus1.add(tangens, 5, 4);
		kujundus1.add(cotangens, 5, 5);

		vaade1 = new Scene(kujundus1, 750, 600);
		vaade2 = new Scene(kujundus2, 750, 500);
		Image image = new Image("file:Morgan.jpg");
		ImageView pildiKuva = new ImageView(image);
		kujundus2.getChildren().addAll(pildiKuva, vaade2to1);
	}

	/**
	 * M22ratakse kalkulaatori nuppude ja v2ljade suurused
	 */
	public void nuppudeSuurused() {
		arvutuseKuvamine.setPrefSize(320, 30);
		vastuseKast.setPrefSize(320, 60);
		numbriNupud[0].setPrefSize(215, 65);
		for (int i = 1; i < 10; i++) {
			numbriNupud[i].setPrefSize(105, 65);
		}
		koma.setPrefSize(105, 65);
		clear.setPrefSize(105, 65);
		korruta.setPrefSize(105, 65);
		lahuta.setPrefSize(105, 65);
		v6rdub.setPrefSize(215, 65);
		plussmin.setPrefSize(105, 65);
		jaga.setPrefSize(105, 65);
		liida.setPrefSize(105, 65);
		siinus.setPrefSize(105, 65);
		koosinus.setPrefSize(105, 65);
		tangens.setPrefSize(105, 65);
		cotangens.setPrefSize(105, 65);
	}

	/**
	 * Luuakse Trigonomeetria klassi p6hjal uus kalkulaatori nimeline objekt. Erinevate
	 * trigonoomia nuppude vajutused suunatakse edasi Trigonoomia klassi
	 * meetoditesse, kus arvutatakse v2lja tehte vastus.
	 * 
	 * Tehte tulemus vormistatakse ja kuvatakse.
	 * 
	 */
	public void arvutaTrigoFunktsioon() {
		Trigonomeetria kalkulaator = new Trigonomeetria();
		siinus.setOnAction(e -> kalkulaator.sinOnVajutatud());
		koosinus.setOnAction(e -> kalkulaator.cosOnVajutatud());
		tangens.setOnAction(e -> kalkulaator.tanOnVajutatud());
		cotangens.setOnAction(e -> kalkulaator.cotOnVajutatud());
		// If tingimus v2ldib 0-i kuvamist programmi k2ivitamisel.
		if (arvutuseKuvamine.getText().length() > 0) {
			Arvutamine.vastuseVormistus();
			Arvutamine.vormistaT2isArv();
			vastuseKast.setText(Double.toString(trigoVastus));
		}
	}
}