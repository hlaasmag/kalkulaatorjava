/**
 * Klassis Arvutamine teostatakse p6hilised arvutamisefunktsioonid,
 * v2lja arvatud trigonomeetriaarvutus, mis teostatakse klassis Trigonomeetria
 */
package kalkulaator;

import java.text.DecimalFormat;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;

/**
 * 
 * Arvutamine klass p2rib Main klassis initseeritud muutujad.
 *
 */
public class Arvutamine extends Main {
	/**
	 * S2testab, mis juhtub, kui vajutada kalkulaatori nuppudele Nupuvajutused
	 * suunatakse vastavatesse meetoditesse, mis m22ravad j2rgneva tegevus.
	 */
	public static void nupuVajutusTegevus() {

		for (int i = 0; i < 10; i++) {
			numbriNupud[i].setOnAction(e -> numbriteTegevus(e));
		}

		koma.setOnAction(e -> komaTegevus(e));
		korruta.setOnAction(e -> tehteNupuVajutus(e));
		jaga.setOnAction(e -> tehteNupuVajutus(e));
		lahuta.setOnAction(e -> tehteNupuVajutus(e));
		liida.setOnAction(e -> tehteNupuVajutus(e));
		plussmin.setOnAction(e -> vahetaM2rki(e));
		clear.setOnAction(e -> puhasta(e));
		v6rdub.setOnAction(e -> arvutaVastus(e));
		vaade2to1.setOnAction(e -> aken.setScene(vaade1));
	}

	/**
	 * M22rame, kuidas salvestatakse ja kuvatakse arvud, kui klikatakse numbreid
	 * 0-9 Arv salvestatkse esimese numbrina, kui tehe on sisestatud,
	 * salvestatakse numbrisisestus teise arvuna.
	 * 
	 * @param e
	 *            - Kasutaja poolt klikatud konkreetse nupu syndmus
	 */
	public static void numbriteTegevus(ActionEvent e) {
		// Kui on juba vajutatud tehtem2rki, salvestab sisestuse
		// teise arvuna
		if (tehteKlikkideArv > 0 && Double.toString(num1).length() != 0) {
			salvestaTeiseNumbrina(e);
		} else {
			salvestaEsimeseNumbrina(e);
		}
	}

	/**
	 * See meetod salvestab kalkulaatorisse sisestatud arvu esimese numbrina
	 * 
	 * @param e
	 */
	public static void salvestaEsimeseNumbrina(ActionEvent e) {
		String vajutatudNupp = ((Button) e.getSource()).getText();
		vastuseKast.setText(vastuseKast.getText() + vajutatudNupp);

		if (vastuseKast.getText().startsWith(".")) {
			vastuseKast.setText("0.");
		} else {
			num1 = Double.parseDouble(vastuseKast.getText());
			// V2ldib probleemi olukorras, kus tehtem2rki on varem kl6psitud
			tehteKlikkideArv = 0;
			tehteSisestusLubatud = true;
		}
	}

	/**
	 * See meetod k2ivitub juhul, kui tehtem2rk on lubatud hetkel sisestatud.
	 * Esimene number on selles olukorras juba salvestatud.
	 * 
	 * @param ActionEvent
	 *            e
	 */
	public static void salvestaTeiseNumbrina(ActionEvent e) {
		String vajutatudNupp = ((Button) e.getSource()).getText();
		vastuseKast.setText(vastuseKast.getText() + vajutatudNupp);
		if (vastuseKast.getText().startsWith(".")) {
			vastuseKast.setText("0.");
		} else {
			num2 = Double.parseDouble(vastuseKast.getText());
			tehteSisestusLubatud = true;
			vastuseNuppLubatud = true;
		}
	}

	/**
	 * Meetod lisab koma vajutusel numbrile komakoha. Kui arv juba sisaldab yhte
	 * koma, siis ei lase meetod seda sisestada.
	 * 
	 * @param e
	 */
	public static void komaTegevus(ActionEvent e) {
		if (vastuseKast.getText().contains(".") || vastuseKast.getText().contains(",")) {
			return;
		} else {
			numbriteTegevus(e);
		}
	}

	/**
	 * 
	 * Meetod s2testab edasise tegevuse kui vajutatakse m6nda
	 * tehtem2rki.(-,+,*,/)
	 * Kui tehte sisestamine on lubatud ja tegemist ei ole tehtem2rgi muutmise
	 * olukorraga, suundub programm edasi meetodisse tehteNupuTegevus
	 * 
	 * @param e
	 */
	public static void tehteNupuVajutus(ActionEvent e) {
		if (tehteSisestusLubatud == true) {
			tehteNupuTegevus(e);
			// Vajutatud tehtem2rgi muutmine.
		} else if (((Button) e.getSource()).getText() != Character.toString(tehe)
				&& arvutuseKuvamine.getText().length() != 0) {
			loeTehteM2rk(e);
			arvutuseKuvamine.setText(Double.toString(num1) + "   " + tehe);
		} else if (tehteSisestusLubatud == false) {
			return;
		}
	}

	/**
	 * Tehte nupu vajutamine on siia j6udmiseks l2binud eelnevad kontrollid.
	 * Meetod kuvab tehtem2rgi vajutamisel senise arvutusk2igu ja vabastab
	 * vastusev2lja uueks sisestuseks.
	 * Kui tehtem2rki vajutatakse olukorras, kus yks tehe on juba sisestatud,
	 * suundutakse uude meetodisse.
	 * 
	 * @param e
	 */
	public static void tehteNupuTegevus(ActionEvent e) {
		// Tegevus juhul, kui yks arvutus on juba sisestatud ja kasutaja soovib
		// edasi arvutada
		if (tehteKlikkideArv > 0) {
			j2tkaArvutamist(e);
			// Tavap2rane olukord, kus yks arv on sisestatud ja vajutatakse
			// tehtem2rki
		} else {
			String arvutusK2iguArv = vastuseKast.getText();
			loeTehteM2rk(e);
			vastuseKast.setText("");
			tehteKlikkideArv++;
			// V2ldib valet kuvamist mitmekorsel klikkimisel
			tehteSisestusLubatud = false;
			arvutuseKuvamine.setText(arvutusK2iguArv + "   " + tehe);
		}
	}

	/**
	 * Tehtem2rgi vajutamine, kui tehe on juba sisestatud, toimib nagu
	 * v6rdusm2rgi vajutamine
	 * Vastus salvestatakse esimeseks arvuks ja kasutaja saab valitud tehtega
	 * edasi arvutada
	 * 
	 * @param e
	 */
	public static void j2tkaArvutamist(ActionEvent e) {
		teheArvutabVastuse(e);
		// Erand juhuks kui siia saabutakse p2rast 0-ga jagamist.
		if (Double.toString(vastus).equals("Infinity")) {
			puhasta(e);
			return;
		}
		vastuseKast.setText("");
		tehteKlikkideArv = 1;
		tehteSisestusLubatud = false;
		loeTehteM2rk(e);
		arvutuseKuvamine.setText(Double.toString(vastus) + "   " + tehe);
		// Vormistab vastuse, kui tegemist on t2isarvuga.
		if (vastus % 1 == 0) {
			String arvutusKastiSisu = arvutuseKuvamine.getText();
			arvutuseKuvamine.setText(arvutusKastiSisu.replace(".0", ""));
		}
	}

	/**
	 * Arvutatakse j2tkaArvutamist meetodi jaoks v2lja vastus
	 * 
	 * @param e
	 */
	public static void teheArvutabVastuse(ActionEvent e) {
		if (tehe == '*') {
			vastus = num1 * num2;
			num1 = vastus;
		} else if (tehe == '/') {
			vastus = num1 / num2;
			num1 = vastus;
			// 0-ga jagamise erand.
			if (num2 == 0) {
				puhasta(e);
				arvutuseKuvamine.setText("");
				aken.setScene(vaade2);
			}
		} else if (tehe == '-') {
			vastus = num1 - num2;
			num1 = vastus;
		} else if (tehe == '+') {
			vastus = num1 + num2;
			num1 = vastus;
		}
	}

	/**
	 * Annab kahtele tehtem2rgi vajutamisele j2rgnevale meetodile vastava
	 * tehtega seotud m2rgi.
	 * 
	 */
	public static void loeTehteM2rk(ActionEvent e) {
		if (e.getSource() == korruta) {
			tehe = '*';
		} else if (e.getSource() == jaga) {
			tehe = '/';
		} else if (e.getSource() == lahuta) {
			tehe = '-';
		} else if (e.getSource() == liida) {
			tehe = '+';
		}
	}

	/**
	 * Teostab m2rgimuutuse ning kuvab uue arvu vastuse kastis.
	 * 
	 * @param e
	 */
	public static void vahetaM2rki(ActionEvent e) {
		if (tehteSisestusLubatud == false || vastuseKast.getText().equals("0") || vastuseKast.getText().equals("0.0")) {
			return;
		} else {
			double vastuseKastiSisu = Double.parseDouble(vastuseKast.getText());
			vastuseKast.setText(Double.toString(vastuseKastiSisu * -1));
			vormistaT2isArv();

			if (tehteKlikkideArv == 0) {
				num1 = Double.parseDouble(vastuseKast.getText());
			} else {
				num2 = Double.parseDouble(vastuseKast.getText());
			}
		}
	}

	/**
	 * "Clear" nupu vajutamisel puhastab kuvamise v2ljad ja nullib muutujad
	 * 
	 * @param e
	 */

	public static void puhasta(ActionEvent e) {
		vastuseKast.setText("");
		num1 = 0;
		num2 = 0;
		tehteKlikkideArv = 0;
		tehteSisestusLubatud = false;
		arvutuseKuvamine.setText("");
	}

	/**
	 * Kui arv vormistatakse .0 l6puga eemaldab .0 l6pu.
	 */
	public static void vormistaT2isArv() {
		try {
			if (Double.parseDouble(vastuseKast.getText()) % 1 == 0) {
				String vastuseKastiSisu = vastuseKast.getText();
				vastuseKast.setText(vastuseKastiSisu.replace(".0", ""));
			}
		} catch (Exception ex) {

		}
	}

	/**
	 * Meetod k2ivitub tehtem2rgi vajutamisel. Teostab vajutatud m2rgiga tehte
	 * ja kuvab ylemisel v2ljal tehtek2igu.
	 * 
	 * @param e
	 */
	public static void arvutaVastus(ActionEvent e) {
		if (vastuseNuppLubatud == true) {
			String teineArv = vastuseKast.getText();
			if (tehe == '*') {
				vastuseKast.setText(Double.toString(num1 * num2));
			} else if (tehe == '/') {
				vastuseKast.setText(Double.toString(num1 / num2));
				// 0-ga jagamise erand
				if (num2 == 0) {
					puhasta(e);
					aken.setScene(vaade2);
					return;
				}
			} else if (tehe == '-') {
				vastuseKast.setText(Double.toString(num1 - num2));
			} else if (tehe == '+') {
				vastuseKast.setText(Double.toString(num1 + num2));
			}
			vastuseVormistus();
			num2 = 0;
			arvutuseKuvamine.setText(arvutuseKuvamine.getText() + "   " + teineArv + " =");
			vormistaT2isArv();

			tehteKlikkideArv = 0;
			tehteSisestusLubatud = true;
			vastuseNuppLubatud = false;
		} else {
			return;
		}
	}

	/**
	 * Kuvab vastuse korrektsemal kujul. Pyyab kinni konverteerimise v6imalikku
	 * vea.
	 */
	public static void vastuseVormistus() {
		vastus = Double.parseDouble(vastuseKast.getText());
		double varuVastus = vastus;
		DecimalFormat ymardamine = new DecimalFormat("#.#########");
		String tulemus = ymardamine.format(vastus);
		vastuseKast.setText(tulemus);
		try {
			num1 = (Double) ymardamine.parse(tulemus);
		} catch (Exception ex) {
			num1 = varuVastus;
		}
	}

}