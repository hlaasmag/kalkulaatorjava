package kalkulaator;

/**
 * Klassis teostatakse trigonomeetrilised arvutused.
 *
 */
public class Trigonomeetria extends Main {
	/**
	 * Konstruktor, mis v6imaldab luua Trigonomeetria objekte.
	 */
	public Trigonomeetria() {

	}

	/**
	 * Siinuse vajutamisel loetakse vastuserealt sisend, mis antakse
	 * arvutaSiinus meetodisse.
	 */
	public void sinOnVajutatud() {
		if (tehteSisestusLubatud == true) {
			trigoSisend = Double.parseDouble(vastuseKast.getText());
			arvutaSiinus(trigoSisend);
		} else {
			return;
		}
	}

	/**
	 * Koosinuse vajutamisel loetakse vastuserealt sisend, mis antakse
	 * arvutaKoosinus meetodisse.
	 */
	public void cosOnVajutatud() {
		if (tehteSisestusLubatud == true) {
			trigoSisend = Double.parseDouble(vastuseKast.getText());
			arvutaKoosinus(trigoSisend);
		} else {
			return;
		}
	}

	/**
	 * Tangensi vajutamisel loetakse vastuserealt sisend, mis antakse
	 * arvutaTangens meetodisse.
	 */
	public void tanOnVajutatud() {
		if (tehteSisestusLubatud == true) {
			trigoSisend = Double.parseDouble(vastuseKast.getText());
			arvutaTangens(trigoSisend);
		} else {
			return;
		}
	}

	/**
	 * Cotangensi vajutamisel loetakse vastuserealt sisend, mis antakse
	 * arvutaCotangens meetodisse.
	 */

	public void cotOnVajutatud() {
		if (tehteSisestusLubatud == true) {
			trigoSisend = Double.parseDouble(vastuseKast.getText());
			arvutaCotangens(trigoSisend);
		} else {
			return;
		}
	}

	/**
	 * Meetod tagastab arvu, millest on v6etud siinus.
	 * 
	 * @param a - arv, millest v6etakse siinus
	 * @return trigoVastus - siinuse arvutamise tulemus
	 */
	public double arvutaSiinus(double a) {
		trigoVastus = Math.sin(Math.toRadians(a));
		vastuseKast.setText(Double.toString(trigoVastus));
		arvutuseKuvamine.setText("sin(" + a + "�)");
		num1 = trigoVastus;
		return trigoVastus;
	}

	/**
	 * Meetod tagastab arvu, millest on v6etud koosinus.
	 * 
	 * @param a - arv, millest v6etakse koosinus
	 * @return trigoVastus - koosinuse arvutamise tulemus
	 */
	public double arvutaKoosinus(double a) {
		trigoVastus = Math.cos((Math.toRadians(a)));
		vastuseKast.setText(Double.toString(trigoVastus));
		arvutuseKuvamine.setText("cos(" + a + "�)");
		num1 = trigoVastus;
		return trigoVastus;
	}

	/**
	 * Meetod tagastab arvu, millest on v6etud tangens
	 * 
	 * @paraam a - arv, millest v6etakse tangens
	 * @return trigoVastus - tangensi arvutamise tulemus
	 */
	public double arvutaTangens(double a) {
		trigoVastus = Math.tan((Math.toRadians(a)));
		vastuseKast.setText(Double.toString(trigoVastus));
		arvutuseKuvamine.setText("tan(" + a + "�)");
		num1 = trigoVastus;
		return trigoVastus;
	}

	/**
	 * Meetod tagastab arvu, millest on v6etud cotangens.
	 * 
	 * @param a - arv, millest v6etakse cotangens
	 * @return trigoVastus - cotangensi arvutamise tulemus
	 */
	public double arvutaCotangens(double a) {
		trigoVastus = Math.tan((Math.toRadians(a)));
		vastuseKast.setText(Double.toString(trigoVastus));
		arvutuseKuvamine.setText("cot(" + a + "�)");
		num1 = trigoVastus;
		return 1 / trigoVastus;
	}
}
